import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

import { NguiMapModule} from '@ngui/map';
import { BodyComponent } from './body/body.component';

@NgModule({
  imports: [
    CommonModule,
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js'})
  ],
  declarations: [LayoutComponent, HeaderComponent, FooterComponent, BodyComponent],
  exports: [LayoutComponent]
})
export class UiModule { }
